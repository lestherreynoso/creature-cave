﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CavePlayerScript : MonoBehaviour {

    public float moveForce = 20f, jumpforce = 700f, maxVelocity = 4f;

    private Rigidbody2D myBody;
    private Animator anim;
    private bool grounded, moveRight, moveLeft;
    void Awake()
    {
        InitVariables();
    }
    void InitVariables()
    {
        myBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        GameObject jumpBtn = GameObject.Find("Jump");
        //if (jumpBtn == null) { Debug.LogWarning("Jump Button not Found"); }
        GameObject.Find("Jump").GetComponent<Button>().onClick.AddListener(() => Jump());
    }
    void PlayerWalkJoystick()
    {
        float forceX = 0f;
        float vel = Mathf.Abs(myBody.velocity.x);

        if (moveRight)
        {
            if (vel < maxVelocity)
            {
                if (grounded)
                {
                    forceX = moveForce;
                }
                else
                {
                    forceX = moveForce * 1.1f;
                }
            }
            transform.localScale = new Vector3(1f, transform.localScale.y, transform.localScale.z);
            anim.SetBool("isWalking", true);
        }
        else if (moveLeft)
        {
            if (vel < maxVelocity)
            {

                if (grounded)
                {
                    forceX = -moveForce;
                }
                else
                {
                    forceX = -moveForce * 1.1f;
                }
            }
            transform.localScale = new Vector3(-1f, transform.localScale.y, transform.localScale.z);
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }
        Debug.Log("x: " + forceX + " y: " + 0);

        myBody.AddForce(new Vector2(forceX, 0));
    }
    void PlayerWalkKeyboard()
    {
        float forceX = 0f;
        float forceY = 0f;
        float vel = Mathf.Abs(myBody.velocity.x);

        float h = Input.GetAxisRaw("Horizontal");

        if (h > 0)
        {
            if (vel < maxVelocity)
            {
                if (grounded)
                {
                    forceX = moveForce;
                }
                else
                {
                    forceX = moveForce * 1.1f;
                }
            }   
            transform.localScale = new Vector3(1f, transform.localScale.y, transform.localScale.z);
            anim.SetBool("isWalking", true);
        }
        else if (h < 0)
        {
            if (vel < maxVelocity)
            {
                if (grounded)
                {
                    forceX = -moveForce;
                }
                else
                {
                    forceX = -moveForce * 1.1f;
                }
            }
            transform.localScale = new Vector3(-1f, transform.localScale.y, transform.localScale.z);
            anim.SetBool("isWalking", true);
        }
        else if (h == 0)
        {
            anim.SetBool("isWalking", false);
        }

        

        if (Input.GetKey(KeyCode.Space))
        {
            if (grounded)
            {
                grounded = false;
                forceY = jumpforce;
            }
        }
        
        myBody.AddForce(new Vector2(forceX, forceY));
     
    }

    public void Jump()
    {
        Debug.Log("Jumping, grounded = " + grounded);
        float forceY = 0f;

        if (grounded)
        {
            grounded = false;
            forceY = jumpforce;
            anim.SetBool("isWalking", false);
            anim.SetBool("isJumping", true);
        }

        myBody.AddForce(new Vector2(0, forceY));


    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //PlayerWalkKeyboard();
        PlayerWalkJoystick();
	}

    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag == "Ground")
        {
            grounded = true;
            anim.SetBool("isJumping", false);
        }
    }

    public void BouncePlayerWithSpring(float force)
    {
        if (grounded) {
            grounded = false;
            myBody.AddForce(new Vector2(0, force));

        }
    }

    public void SetMoveLeft(bool moveLeft)
    {
        this.moveLeft = moveLeft;
        this.moveRight = !moveLeft;
    }

    public void StopMoving()
    {
        this.moveLeft = false;
        this.moveRight = false;
    }
}
