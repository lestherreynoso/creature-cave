﻿using UnityEngine;
using System.Collections;

public class TimeAndAirScript : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Player")
        {
            if (gameObject.name == "Time Collectable")
            {
                GameObject.Find("Gameplay Controller").GetComponent<LevelTimer>().time += 15f;

            }
            else
            {
                GameObject.Find("Gameplay Controller").GetComponent<AirTimer>().air += 15f;
            }

            Destroy(gameObject);
        }
        
    }

}
