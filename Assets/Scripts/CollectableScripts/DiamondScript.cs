﻿using UnityEngine;
using System.Collections;

public class DiamondScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        if (CaveDoor.instance != null)
            CaveDoor.instance.collectablesCount++;

        Debug.Log(CaveDoor.instance.collectablesCount);
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Player")
        {
            Destroy(gameObject);
            if(CaveDoor.instance != null)
                CaveDoor.instance.DecrementCollectables();
        }
    }

}
