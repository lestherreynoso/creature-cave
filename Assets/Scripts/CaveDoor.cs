﻿using UnityEngine;
using System.Collections;

public class CaveDoor : MonoBehaviour {

    public static CaveDoor instance;

    private Animator anim;
    private BoxCollider2D box;

    [HideInInspector]
    public int collectablesCount;

    void Awake()
    {
        MakeInstance();
        anim = GetComponent<Animator>();
        box = GetComponent<BoxCollider2D>();
    }

    void MakeInstance()
    {
        if (instance == null)
            instance = this;
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void DecrementCollectables()
    {
        collectablesCount--;
        Debug.Log("Collectable count: " + collectablesCount);
        if (collectablesCount == 0)
        {
            StartCoroutine(OpenDoor());
        }
    }

    IEnumerator OpenDoor()
    {
        anim.SetBool("isLocked", false);
        anim.SetBool("isOpen", true);

        yield return new WaitForSeconds(.7f);
        box.isTrigger = true;

    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Player")
        {
            Debug.Log("Game Finished");
            GameObject.Find("Gameplay Controller").GetComponent<GameplayController>().PlayerDied();
        }
    }

}
