﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelTimer : MonoBehaviour {

    private Slider slider;

    private GameObject player;
    public float time = 10f;
    public float timeBurn = 2f;

    void Awake()
    {
        InitVariables();
    }

    void InitVariables()
    {
        player = GameObject.Find("AlienBiege - Cave");
        slider = GameObject.Find("Time Slider").GetComponent<Slider>();

        slider.minValue = 0f;
        slider.maxValue = time;
        slider.value = slider.maxValue;

    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!player) return;
        if (time > 0)
        {
            time -= timeBurn * Time.deltaTime;
            slider.value = time;

        }
        else
        {
            GetComponent<GameplayController>().PlayerDied();
            Destroy(player);
        }
    }
}
