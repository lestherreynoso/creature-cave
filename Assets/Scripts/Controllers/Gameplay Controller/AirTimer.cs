﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AirTimer : MonoBehaviour {

    private Slider slider;

    private GameObject player;
    public float air = 10f;
    public float airBurn = 1f;

    void Awake()
    {
        InitVariables();
    }

    void InitVariables()
    {
        player = GameObject.Find("AlienBiege - Cave");
        slider = GameObject.Find("Air Slider").GetComponent<Slider>();

        slider.minValue = 0f;
        slider.maxValue = air;
        slider.value = slider.maxValue;

    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (!player) return;
        if (air > 0)
        {
            air -= airBurn * Time.deltaTime;
            slider.value = air;

        }
        else
        {
            GetComponent<GameplayController>().PlayerDied();
            Destroy(player);
        }
	}
}
