﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelSelectController : MonoBehaviour {

    public void BackToMenu()
    {
        SceneManager.LoadScene("Creature Cave Main Menu");
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("Cave Level 01");
    }
}
