﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuController : MonoBehaviour {

    public void SelectLevel(){
        SceneManager.LoadScene("Creature Cave Level Select Menu");
    }
}
