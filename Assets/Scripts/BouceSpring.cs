﻿using UnityEngine;
using System.Collections;

public class BouceSpring : MonoBehaviour
{

    public float force = 500f;
    private Animator anim;

    // Use this for initialization
    void Awake()
    {

        anim = GetComponent<Animator>();
    }
    IEnumerator AnimateBounce()
    {
        anim.Play("spring bounce");
        yield return new WaitForSeconds(.5f);
        anim.Play("spring down");
    }
    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Player")
        {
            target.gameObject.GetComponent<CavePlayerScript>().BouncePlayerWithSpring(force);
            StartCoroutine(AnimateBounce());
        }
    }
}
