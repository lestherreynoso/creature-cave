﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Joystick : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    private CavePlayerScript player;

    void Start()
    {
        player = GameObject.Find("AlienBiege - Cave").GetComponent<CavePlayerScript>();
    }
    public void OnPointerDown(PointerEventData data)
    {
        if (gameObject.name == "Left")
        {
            //Debug.Log("Touching left: " + data);
            player.SetMoveLeft(true);
        }
        else if (gameObject.name == "Right")
        {
            //Debug.Log("Touching right: " + data);
            player.SetMoveLeft(false);

        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        player.StopMoving();

        /*
        if (gameObject.name == "Left")
        {
            Debug.Log("Let go left: " + data);
        }
        else if (gameObject.name == "Right")
        {
            Debug.Log("Let go right: " + data);
        }
        */
    }
}
