﻿using UnityEngine;
using System.Collections;

public class SpiderShooter : MonoBehaviour {

    [SerializeField]
    private GameObject bullet;



	// Use this for initialization
	void Start () {
        StartCoroutine(Attack());
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator Attack()
    {
        yield return new WaitForSeconds(Random.Range(2f, 7f));

        Instantiate(bullet, transform.position, Quaternion.identity);
        StartCoroutine(Attack());

    }
    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Player")
        {
            Destroy(target.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag == "Player")
        {
            Destroy(target.gameObject);
            GameObject.Find("Gameplay Controller").GetComponent<GameplayController>().PlayerDied();
        }
    }
}
