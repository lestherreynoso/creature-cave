﻿using UnityEngine;
using System.Collections;

public class SpiderJumper : MonoBehaviour {

    public float forceY = 300f;
    private Rigidbody2D myBody;
    private Animator anim;

    void Awake()
    {
        InitVariables();
    }
    void InitVariables()
    {
        myBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    IEnumerator Attack()
    {
        yield return new WaitForSeconds(Random.Range(2f, 7f));
        forceY = Random.Range(250f, 550f);

        myBody.AddForce(new Vector2(0, forceY));
        anim.SetBool("isAttacking", true);
        yield return new WaitForSeconds(.7f);
        StartCoroutine(Attack());

    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Ground")
        {
            anim.SetBool("isAttacking", false);
        }

    }

    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag == "Player")
        {
            Destroy(target.gameObject);
            GameObject.Find("Gameplay Controller").GetComponent<GameplayController>().PlayerDied();
        }
    }
}
