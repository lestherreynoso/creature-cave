﻿using UnityEngine;
using System.Collections;

public class SpiderWalker : MonoBehaviour {

    public float speed = 1f;
    private Rigidbody2D myBody;
    private Animator anim;

    [SerializeField]
    private Transform startPos, endPos;

    private bool collision;
   

    void Awake()
    {
        InitVariables();
    }
    void InitVariables()
    {
        myBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
        anim.SetBool("isMoving", true);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Move();
        ChangeDirection();
	}

    void Move()
    {
        myBody.velocity = new Vector2(transform.localScale.x, 0) * speed;
    }

    void ChangeDirection()
    {
        collision = Physics2D.Linecast (startPos.position, endPos.position, 1 << LayerMask.NameToLayer("Ground"));
        Debug.Log(collision);
        Debug.DrawLine(startPos.position, endPos.position, Color.green);
        if (!collision)
        {
            Vector3 temp = transform.localScale;
            if (temp.x > 0)
            {
                temp.x = -transform.localScale.x;
            }
            else
            {
                temp.x = Mathf.Abs(transform.localScale.x);
            }

            transform.localScale = temp;
        }
    }

    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag == "Player")
        {
            Destroy(target.gameObject);
            GameObject.Find("Gameplay Controller").GetComponent<GameplayController>().PlayerDied();
        }
    }
}
