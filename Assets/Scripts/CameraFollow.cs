﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public float minx, maxx;
    private Transform player;
	// Use this for initialization
	void Start () {
        player = GameObject.Find("AlienBiege - Cave").transform;
       
	}
	
	// Update is called once per frame
	void Update () {
        if (player != null)
        {
            Vector3 temp = transform.position;
            temp.x = Mathf.Clamp(player.position.x, minx, maxx);
            
            temp.y = player.position.y + 3.5f;
            transform.position = temp;
        }
	}
}
